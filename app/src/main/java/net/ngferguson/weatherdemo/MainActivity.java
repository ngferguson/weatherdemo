package net.ngferguson.weatherdemo;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class MainActivity extends Activity implements LocationListener {

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getLocation();
        showWeather();
    }

    private void showWeather(){
        //final TextView tv = (TextView) findViewById(R.id.latlong);
        //tv.setText(latitude + ", " + longitude);

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String baseUrl = getString(R.string.base_api_url);
                    URL url = new URL(baseUrl + latitude + "," + longitude);

                    HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();

                    final int statusCode = urlConnection.getResponseCode();

                    if(statusCode == HttpsURLConnection.HTTP_OK) {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                        final String jsonResponseString = reader.readLine();
                        JSONObject json = new JSONObject(jsonResponseString);
                        JSONObject current = json.getJSONObject("currently");

                        final int temperature = current.getInt("temperature");
                        final String summary = current.getString("summary");
                        final int humidity = (int)(current.getDouble("humidity") * 100);
                        final int wind = current.getInt("windSpeed");
                        final int precipProbability = (int)(current.getDouble("precipProbability") * 100);
                        final String icon = current.getString("icon");

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final TextView summaryTextView = (TextView) findViewById(R.id.summaryTextBox);
                                summaryTextView.setText(summary);

                                final TextView tempTextView = (TextView) findViewById(R.id.tempDisplay);
                                tempTextView.setText(temperature + "°F");

                                final TextView humidityTextView = (TextView) findViewById(R.id.humidityDisplay);
                                humidityTextView.setText(humidity + "%");

                                final TextView windTextView = (TextView) findViewById(R.id.windDisplay);
                                windTextView.setText(wind + " mph");

                                final TextView precipView = (TextView) findViewById(R.id.precipDisplay);
                                precipView.setText(precipProbability + "%");

                                ImageView iconView = (ImageView) findViewById(R.id.icon);
                                if(icon.equals("clear-day"))
                                    iconView.setImageResource(R.drawable.sun);
                                else if(icon.equals("rain"))
                                    iconView.setImageResource(R.drawable.rain);
                                else if(icon.equals("snow"))
                                    iconView.setImageResource(R.drawable.snow);
                                else if(icon.equals("wind"))
                                    iconView.setImageResource(R.drawable.wind);
                                else
                                    iconView.setImageResource(R.drawable.pcloudy);
                            }
                        });
                    } else {


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final TextView summaryTextView = (TextView) findViewById(R.id.summaryTextBox);
                                summaryTextView.setText("HTTP Error: " + statusCode);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    private void getLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, R.id.LOCATION_PERMISSION);
                return;
            }
        }
        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case R.id.LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getLocation();
                    showWeather();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
